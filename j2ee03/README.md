* JSP - Java Server Pages (an introduction)
    * clock.jsp:        scriptlets and markup in a messy mixup
    * otherClock.jsp:   Clock logic is encapsulated in the ClockBean class. JSP actions are fetching epoch time in millis and in an human readable form
    * with-tags.jsp:    A JSP using some standard tags.
