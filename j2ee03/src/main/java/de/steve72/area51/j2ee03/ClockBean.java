package de.steve72.area51.j2ee03;

import javax.inject.Named;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * ClockBean
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Named
public class ClockBean {

    private String dateFormat = "dd.MM.yyyy HH:mm:ss,SSSS";


    public ClockBean() {

    }


    public long getCurrentTimeSinceEpoch() {

        return System.currentTimeMillis();
    }


    public String getHumanReadableDate() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);

        LocalDateTime localDateTime = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault()
        );

        return localDateTime.format(dtf);
    }


    public String getDateFormat() {

        return dateFormat;
    }


    public void setDateFormat(String dateFormat) {

        this.dateFormat = dateFormat;
    }
}
