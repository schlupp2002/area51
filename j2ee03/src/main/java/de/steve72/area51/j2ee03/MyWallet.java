package de.steve72.area51.j2ee03;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * MyWallet
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class MyWallet {

    private String[] coins = {"1c", "1c", "2c", "5c", "10c"};

    private String[] notes = {"1$", "5$", "10$", "10$", "10$", "20$"};

    private String[] receipts = {"gas - $42.50", "grocery - $35.26", "bookstore - $12.99"};


    public String[] getCoins() {

        return coins;
    }


    public List getNotes() {

        return Arrays.asList(notes);
    }


    public Set getReceipt() {

        return new HashSet(Arrays.asList(receipts));
    }
}
