package de.steve72.area51.j2ee03;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * MyClockBeanTag
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class MyClockBeanTag extends SimpleTagSupport {

    private String dateFormat = "dd.MM.yyyy HH:mm:ss,SSSS";


    public String getDateFormat() {

        return dateFormat;
    }


    public void setDateFormat(String dateFormat) {

        this.dateFormat = dateFormat;
    }

    // this method is the key part of jsp custom tags


    @Override
    public void doTag() throws JspException, IOException {

        JspWriter out = getJspContext().getOut();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);

        LocalDateTime localDateTime = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault());

        out.print(localDateTime.format(dtf));
    }
}
