<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="custom" uri="/WEB-INF/myCustomTags.tld" %>

<!DOCTYPE html>

<html>

<head>
    <title>JSP /w custom tags</title>
</head>

<body>

    <div align="center">
        <h2>Custom tags</h2>
        <p>
            Here is the current time
            <custom:formatted-date dateFormat="q/MM.yyyy a"/>
        </p>
    </div>

</body>

</html>
