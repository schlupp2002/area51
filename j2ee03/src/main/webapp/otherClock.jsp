<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <title>Clock with Bean</title>
</head>

<body>

<!-- Bean is referenced by fully qualified class name -->
<jsp:useBean
        id="myClockBean"
        class="de.steve72.area51.j2ee03.ClockBean"
        scope="page"/>

<div align="center">

    <h2>Hello there!</h2>

    <p>
        It's been
        <br/>

        <!-- get the milliseconds from the backing bean -->
        <jsp:getProperty name="myClockBean" property="currentTimeSinceEpoch"/>

        <br/>
        milliseconds since midnight, January 1970
    </p>

    <p>
        In other words, it's
        <br/>

        <!-- get the human readable date from the backing bean -->
        <jsp:getProperty name="myClockBean" property="humanReadableDate"/>

    </p>

    <p>

        And now the same datetime in another format
        <br/>

        <!-- set the formatting string within the ClockBean -->
        <jsp:setProperty name="myClockBean" property="dateFormat" value="q uu HH:mm:ss"/>

        <!-- and get the human readable date again -->
        <jsp:getProperty name="myClockBean" property="humanReadableDate"/>

    </p>

    <p>

        And now the same datetime formatted by an URL GET parameter
        <br/>
        <!-- transmit the GET parameter to a property -->
        <jsp:setProperty name="myClockBean" property="dateFormat" param="frm"/>

        <!-- and get the human readable date again -->
        <jsp:getProperty name="myClockBean" property="humanReadableDate"/>

    </p>

</div>

</body>
</html>
