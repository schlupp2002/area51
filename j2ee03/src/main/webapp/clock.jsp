<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.ZoneId" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.Instant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <title>JSP clock</title>
</head>

<body>

<div align="center">
    <h2>Hello there!</h2>
    <p>It's been <%=System.currentTimeMillis() %> since midnight, January 1970</p>
    <p>
        In other words, it's
        <%
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss,SSSS");
            LocalDateTime now = LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()),ZoneId.systemDefault());

            out.println(now.format(dtf));
        %>
    </p>
</div>
</body>
</html>
