<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>

<head>
    <title>JSP /w JSTL</title>
</head>

<body>

<jsp:useBean id="myClock" class="de.steve72.area51.j2ee03.ClockBean"/>
<jsp:useBean id="myWallet" class="de.steve72.area51.j2ee03.MyWallet"/>

<div align="center">

    <h2>c:foreach as for 1..n </h2>
    <p>
        <c:forEach var="i" begin="1" end="20" step="2">
            ${i}&nbsp;
        </c:forEach>
    </p>

    <h2>c:foreach as foreach</h2>
    <p>
        <b>coins:</b>
        <c:forEach var="coin" items="${myWallet.coins}">
            ${coin}&nbsp;
        </c:forEach>
    </p>

    <p>
        <b>notes:</b>
        <c:forEach var="note" items="${myWallet.notes}">
            ${note}
        </c:forEach>
    </p>

    <p>
        <b>receipts:</b>
        <c:forEach var="receipt" items="${myWallet.receipt}">
            ${receipt}
        </c:forEach>
    </p>

    <h2>c:if ...</h2>
    <p>
        <b>we get any tea in the afternoon. we get any coffee in the morning</b>
        <br/>

        <!-- configure the output format of ClockBean.humanReadableDate() -->
        <jsp:setProperty name="myClock" property="dateFormat" value="a"/>


        <c:if test="${myClock.humanReadableDate=='PM'}">
            time for tea!
        </c:if>

        <c:if test="${myClock.humanReadableDate=='AM'}">
            time for coffee!
        </c:if>

    </p>

</div>

</body>

</html>
