package de.steve72.area51.j2ee01;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * DisplayPhotoServlet
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */


@WebServlet(name = "DisplayPhotoServlet", urlPatterns = {"/DisplayPhotoServlet"})
public class DisplayPhotoServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {

        // method body to configure the servlet before it is put into service
    }


    @Override
    public void destroy() {

        System.out.printf("destroy %s\n", this.getClass().getSimpleName());
        // method body to clean up before the garbage collector is doing its job
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String indexString = req.getParameter("photo");

        // extracting the picture index from the URL GET attribute "photo"
        int index = (new Integer(indexString.trim())).intValue();

        // setting the response content type
        resp.setContentType("image/jpeg");

        try (OutputStream out = resp.getOutputStream()) {

            ServletContext myServletContext = req.getServletContext();

            // getting the registrated photoalbum from the context
            PhotoAlbum pa = PhotoAlbum.getPhotoAlbum(myServletContext);

            // fetching picture's binary data
            byte[] bytes = pa.getPhotoData(index);

            // writing picture's binary data to the repsonse output stream
            for (int i = 0; i < bytes.length; i++) {
                out.write(bytes[i]);
            }
        }
    }
}
