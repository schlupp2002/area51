package de.steve72.area51.j2ee01;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * RemovePhotoServlet
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@WebServlet(name = "RemovePhotoServlet", urlPatterns = {"/RemovePhotoServlet"})
public class RemovePhotoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String indexString = req.getParameter("photo");
        int index = (new Integer(indexString.trim())).intValue();

        PhotoAlbum pa = PhotoAlbum.getPhotoAlbum(req.getServletContext());
        pa.removePhoto(index);

        RequestDispatcher rd = req.getRequestDispatcher("DisplayAlbumServlet");
        rd.forward(req, resp);
    }
}
