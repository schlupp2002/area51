package de.steve72.area51.j2ee02;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.CollationKey;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * DisplayAlbumServlet
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@WebServlet(name = "steve", urlPatterns = {"/DisplayAlbumServlet"})
@MultipartConfig()
public class DisplayAlbumServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        handleRequest(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        handleRequest(req, resp);
    }


    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        ServletContext servletContext = request.getServletContext();

        PhotoAlbum pa = PhotoAlbum.getPhotoAlbum(request.getSession());

        if (request.getContentType() != null && request.getContentType().startsWith("multipart/form-data")) {

            this.uploadPhoto(request, pa);
        }

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try (PrintWriter printWriter = response.getWriter()) {
            printWriter.write("<html>");
            printWriter.write("<head>");
            printWriter.write("<title>Photo Viewer</title>");
            printWriter.write("</head>");
            printWriter.write("<body>");
            printWriter.write("<h3 align='center'>Photos</h3>");
            printWriter.printf("<p>Bilder: %d</p>", pa.getPhotoCount());

            LocalDateTime sessionTime =
                    LocalDateTime.ofInstant(
                            Instant.ofEpochMilli(request.getSession().getLastAccessedTime()),
                            ZoneId.systemDefault());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            printWriter.printf("<p>Session: %s, max. Inactive Interval  %ds,  last accessed at: %s</p>",
                    request.getSession().getId(),
                    request.getSession().getMaxInactiveInterval(),
                    sessionTime.format(formatter));

            this.displayAlbum(pa, "", printWriter);

            printWriter.println("</body>");
            printWriter.print("</html>");
        }
    }


    private void uploadPhoto(HttpServletRequest request, PhotoAlbum pa) throws IOException, ServletException {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String fileName = "";

        for (Part p : request.getParts()) {

            this.copyBytes(p.getInputStream(), byteArrayOutputStream);
            fileName = p.getSubmittedFileName();
        }

        if (!fileName.equals("")) {

            String photoName = fileName.substring(0, fileName.lastIndexOf("."));
            pa.addPhoto(photoName, byteArrayOutputStream.toByteArray());
        }
    }


    private void displayAlbum(PhotoAlbum pa, String label, PrintWriter writer) {

        writer.write("<h3 align='center'>" + label + "</h3>");
        writer.write("<table align='center'>");

        for (int j = 0; j < pa.getPhotoCount(); j++) {

            writer.write("<td>");
            writer.printf("<a href='./DisplayPhotoServlet?photo=%d'>", j);
            writer.printf("<img src='./DisplayPhotoServlet?photo=%d' alt='photo' height='120'>", j);
            writer.write("</a>");
            writer.println("</td>");
        }

        writer.write("<td bgcolor='#ccc' width='120' height='120'>");
        writer.write("<form align='left' action='DisplayAlbumServlet' method='POST' enctype='multipart/form-data'>");
        writer.write("<input type='file' value='choose' name='myFile' accept='image/jpeg'></br>");
        writer.write("<input type='submit' value='upload'></br>");
        writer.write("</form>");
        writer.write("</td>");
        writer.write("</tr>");

        writer.write("<tr>");
        for (int j = 0; j < pa.getPhotoCount(); j++) {

            writer.write("<td align='center'>");
            writer.write(pa.getPhotoName(j));
            writer.write("</td>");
        }
        writer.write("</tr>");

        writer.write("<tr>");
        for (int j = 0; j < pa.getPhotoCount(); j++) {

            writer.write("<td align='center'>");
            writer.printf("<a href='RemovePhotoServlet?photo=%d'>remove</a>", j);
            writer.write("</td>");
        }
        writer.write("</tr>");
        writer.write("</table>");

    }


    private void copyBytes(InputStream is, OutputStream os) throws IOException {

        int i = 0;

        while ((i = is.read()) != -1) {
            os.write(i);
        }

        is.close();
        os.close();
    }
}
