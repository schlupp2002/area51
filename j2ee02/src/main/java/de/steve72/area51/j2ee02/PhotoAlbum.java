package de.steve72.area51.j2ee02;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * PhotoAlbum
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class PhotoAlbum {

    /**
     * an identifier used for registration purposes
     */
    public static String ATTRIBUTE_NAME = "Photo_Album";

    /**
     * the local repository
     */
    private List<byte[]> photoDataList = new ArrayList<>();

    /**
     * the names of the photos in the repository
     */
    private List<String> names = new ArrayList<>();


    /**
     * Liefert ein PhotoAlbum als neue Instanz oder aus dem ServletContext heraus.
     * </p>
     *
     *
     * @param httpSession
     * @return
     */
    public static PhotoAlbum getPhotoAlbum(HttpSession httpSession) {

        if (httpSession.getAttribute(ATTRIBUTE_NAME) == null) {

            PhotoAlbum pa = new PhotoAlbum();
            httpSession.setAttribute(ATTRIBUTE_NAME, pa);
        }

        return (PhotoAlbum) httpSession.getAttribute(ATTRIBUTE_NAME);
    }


    /**
     * Fügt eine Photo zum Album hinzu.
     * </p>
     *
     * @param name
     * @param bytes
     */
    public synchronized void addPhoto(String name, byte[] bytes) {

        this.photoDataList.add(bytes);
        this.names.add(name);
    }


    /**
     * Liefert ein Photo aus dem lokalen Repository.
     * </p>
     *
     * @param i
     * @return
     */
    public synchronized byte[] getPhotoData(int i) {

        return (byte[]) photoDataList.get(i);
    }


    /**
     * Liefert zu einem Photoindex den Namen des Photos.
     * </p>
     *
     * @param i
     * @return
     */
    public synchronized String getPhotoName(int i) {

        return (String) names.get(i);
    }


    /**
     * Liefert die Größe des Photo-Repositorys.
     * </p>
     *
     * @return
     */
    public synchronized int getPhotoCount() {

        return photoDataList.size();
    }


    /**
     * Method removes a Photo and it's name from the local repository.
     * </p>
     *
     * @param i
     */
    public synchronized void removePhoto(int i) {

        photoDataList.remove(i);
        names.remove(i);
    }
}
