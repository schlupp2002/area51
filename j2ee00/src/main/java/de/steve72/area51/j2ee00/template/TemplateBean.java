package de.steve72.area51.j2ee00.template;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * TemplateBean
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Named
@SessionScoped
public class TemplateBean implements Serializable {

    private static final long serialVersionUID = -8196734457020450544L;

    private String contract = "white";


    public String getContract() {

        return contract;
    }


    public void setContract(String contract) {

        this.contract = contract;
    }
}
