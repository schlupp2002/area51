package de.steve72.area51.j2ee04;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * HelloBean
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Named("myHelloBean")
@RequestScoped
public class HelloBean {

    private String name = "dear reader";


    public HelloBean() {

    }


    public String getName() {

        return name;
    }


    public void setName(String name) {

        this.name = name;
    }
}
