package de.steve72.area51.j2ee04.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * MyValidator checks wether the submitted string is shorter than 5 characters
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@FacesValidator("myValidator")
public class MyValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        String test = (value instanceof String) ? (String) value : "";

        if (test.length() < 5)
            throw new ValidatorException(new FacesMessage("myValidation fails because the string is too short"));
    }
}
