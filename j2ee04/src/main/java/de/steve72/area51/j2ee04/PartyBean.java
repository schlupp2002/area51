package de.steve72.area51.j2ee04;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * PartyBean
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Named("partyBean")
@SessionScoped
public class PartyBean implements Serializable {

    private static final long serialVersionUID = -646209136527755667L;

    private String name;

    private boolean parentsAllowed;

    private List<Guest> guests;

    private String imageUri;


    public PartyBean() {

        reset();
    }


    public void reset() {

        this.name = "(party title)";
        this.parentsAllowed = false;
        this.guests = new ArrayList<>();
        guests.add(new Guest("me", 44));
        this.imageUri = "party1.jpg";
    }


    public String getSummary() {

        StringBuilder sb = new StringBuilder();

        sb.append("My party is called " + name);
        sb.append(", ");
        sb.append("they're " + guests.size() + " guests");
        sb.append(", ");
        sb.append("and parents are " + (parentsAllowed ? "" : "not ") + " allowed to stay.");

        return sb.toString();
    }


    public String getName() {

        return name;
    }


    public void setName(String name) {

        this.name = name;
    }


    public boolean isParentsAllowed() {

        return parentsAllowed;
    }


    public void setParentsAllowed(boolean parentsAllowed) {

        this.parentsAllowed = parentsAllowed;
    }


    public List<Guest> getGuests() {

        return guests;
    }


    public void setGuests(List<Guest> guests) {

        this.guests = guests;
    }


    public String getImageUri() {

        return "/images/" + imageUri;
    }


    public void setImageUri(String imageUri) {

        this.imageUri = imageUri;
    }
}
