package de.steve72.area51.j2ee04.listener;

import de.steve72.area51.j2ee04.CountryCodes;

import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;

/**
 * MyCountryValueChangeListener
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class MyCountryValueChangeListener implements ValueChangeListener {

    @Override
    public void processValueChange(ValueChangeEvent event) throws AbortProcessingException {

        //access country bean directly
        CountryCodes country = (CountryCodes) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("countryCodes");

        if (country != null)
            country.setLocaleCode(event.getNewValue().toString());
    }
}
