package de.steve72.area51.j2ee04;

/**
 * Guest
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class Guest {

    private String name;
    private int age;


    public Guest(String name, int age) {

        this.name = name;
        this.age = age;
    }


    @Override
    public String toString() {

        return "Guest: " + name;
    }


    public String getName() {

        return name;
    }


    public void setName(String name) {

        this.name = name;
    }


    public int getAge() {

        return age;
    }


    public void setAge(int age) {

        this.age = age;
    }
}
