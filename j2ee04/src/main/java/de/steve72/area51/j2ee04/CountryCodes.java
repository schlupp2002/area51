package de.steve72.area51.j2ee04;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * CountryCodes
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Named("countryCodes")
@SessionScoped
public class CountryCodes implements Serializable {

    private static final long serialVersionUID = 6504495729789961997L;

    // holds country names as keys and codes as values
    private  Map<String, String> countries;

    // holds the current countrycode
    private String localeCode = "en"; //default value


    @PostConstruct
    public void init(){

        countries = new LinkedHashMap<String, String>();

        countries.put("United Kingdom", "en"); //label, value
        countries.put("French", "fr");
        countries.put("German", "de");
        countries.put("China", "zh_CN");
    }


    public CountryCodes() {

    }


    public void countryLocaleCodeChanged(ValueChangeEvent e) {

        //assign new value to localeCode
        localeCode = e.getNewValue().toString();

    }


    // region getters & setters
    public  Map<String, String> getCountryInMap() {

        return countries;
    }

    public String getLocaleCode() {

        return localeCode;
    }


    public void setLocaleCode(String localeCode) {

        this.localeCode = localeCode;
    }
    // endregion
}
