package de.steve72.area51.j2ee04;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * ScopeTestConversation
 *
 * Scope ConversationScoped is used by party.xhtml. Take a look at the session id
 * while starting/stopping the conversation. A conversation lives within a session
 * and is independent from it.
 * <p/>
 * In this case the conversation starts after the constructor was called and ends
 * when the method finishInteractions was triggered by the UI.
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Named("conversation")
@ConversationScoped
public class ScopeTestConversation implements Serializable {


    private static final long serialVersionUID = 1199344603553678256L;

    @Inject
    Conversation conversation;

    private String memory;


    @PostConstruct
    public void init() {

        if (conversation.isTransient())
            conversation.begin();
    }


    public ScopeTestConversation() {

        memory = "fresh conversation";
    }


    public String getMemory() {

        return memory;
    }


    public void setMemory(String memory) {

        this.memory = memory;
    }


    public void finishInteractions() {

        if (!conversation.isTransient())
            conversation.end();
    }
}
